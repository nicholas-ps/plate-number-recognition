import cv2
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.cross_validation import train_test_split
from sklearn.externals import joblib

def process_image_filtered_threshold(img):
    img = cv2.blur(img, (5, 5))
    cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU,img)
    return img

def extract_image_to_contours(img):
    contours, hier = cv2.findContours(img, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    contours = sorted(contours, key=lambda ctr: cv2.boundingRect(ctr)[0])
    return contours

def image_to_feature_vector(image, size=(35, 70)):
    # resize the image to a fixed size, then flatten the image into
	# a list of raw pixel intensities
    return cv2.resize(image, size)

def filter_contour(ctr, img):
    # Get bounding box
    x, y, w, h = cv2.boundingRect(ctr)
    if (h < 90):
        return None
    # Tinggi karakter sekitar 100
    # Lebar karakter sekitar 20 - 50
    elif (w > 100 or w < 10):
        return None
    # Getting ROI
    
    roi = img[y:y+h, x:x+w]
    
    return roi

def classify_plate_number(img_path):
    img = cv2.imread(img_path, 0)
    print('masuk classify')
    print(img)
    img = process_image_filtered_threshold(img)
    contours = extract_image_to_contours(img)
    character_list = []

    for i, ctr in enumerate(contours):
        roi = filter_contour(ctr, img)
        
        try:
            roi.any()
        except:
            continue
        
        roi = image_to_feature_vector(roi)
        cv2.imwrite('test_segmented_chars/character_%d.png'%i, roi)
        roi = roi.flatten()
        character_list.append(roi)
    
    print(character_list)

    knn = joblib.load('knn_model.pkl')
    predictions = knn.predict(character_list)
    
    print(predictions)

    return predictions