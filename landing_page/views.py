import os
import subprocess
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from plate_number_classifier import classify_plate_number
from .models import plate_image

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../"

# Create your views here.
def index(request):
    html = "landing_page.html"
    response = {}
    return render(request, html, response)

def submit(request):
    if request.method == "POST" and len(request.FILES) > 0:
        uploaded_img = request.FILES["image"]
        plate = plate_image.objects.create(img_path=uploaded_img)
        img_path = os.path.join(PROJECT_ROOT, str(plate.img_path))
        result = classify_plate_number(img_path)
        plate.delete()

        #delete
        cmd = ("rm -r " + PROJECT_ROOT + "plates/").split()
        subprocess.run(cmd)

        #render
        html = "result.html"
        response = {
            "result" : "".join(result).upper()
        }
        return render(request, html, response)
    else:
        return HttpResponseRedirect(
            reverse("index")
        )
