from django.urls import path
from .views import index, submit

urlpatterns = [
    path('landing-page', index, name='index'),
    path('submit', submit, name="submit"),
]
